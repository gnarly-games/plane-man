using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;

public class GameManager : MonoBehaviour
{
    public static GameManager manager;
    public CameraManager _cameraManager;
    public TextMeshProUGUI _scoreTextIntro;
    public TextMeshProUGUI _scoreTextMain;
    public TextMeshProUGUI _scoreTextEnd;
    public TextMeshProUGUI _highScoreTextEnd;
    
    public GameObject introCanvas;
    public GameObject inGameCanvas;
    public GameObject deadGameCanvas;
    public GameObject endGameCanvas;
    public TMP_Text inGameLevelText;
    public GameObject collectablePrefab;
    public GameObject upgradePrefab;
    public GameObject obstacklePrefab;
    public GameObject speedBoostPrefab;

    public List<GameObject> collectables;
    public List<GameObject> upgrades;
    public List<GameObject> windowTypes;
    public List<GameObject> windows;
    public List<GameObject> speedBoosts;
    public PlayerController playerController;
    public Transform player;
    public int health = 3;
    public int score=0;
    public int highScore=0;
    private int level = 1;
    public enum GameState
    {
        Prepare,
        MainGame,
        DeadGame,
        EndGame
    }

    private GameState _currentGameState;
    public GameState CurrentGameState
    {
        get { return _currentGameState; }
        set
        {
            switch (value)
            {
                case GameState.Prepare:
                    break;
                case GameState.MainGame:
                    break;
                case GameState.DeadGame:
                    break;
                case GameState.EndGame:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }

            _currentGameState = value;
        }
    }
    private void Awake()
    {
        manager = this;
    }

    private void Start()
    {
        SpawnCoins();
        SpawnUpgrades();
        SpawnWindows();
        playerController.IdleAnimation();
    }

    private void Update()
    {
        UpScore();
        switch (GameManager.manager.CurrentGameState)
        {
            case GameManager.GameState.Prepare:
                //resetleme ve
                //level yükleme aşaması - instantiate
                break;
            case GameManager.GameState.MainGame:
                playerController.PlayerMove();
                playerController.SliderMove();
                break;
            case GameManager.GameState.DeadGame:
                break;
            case GameManager.GameState.EndGame:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void playerResetPos()
    {
        player.localPosition = Vector3.zero;
        
    }
     public void UpScore()
     {
         if (score<0)
         {
             score = 0;
         }
         _scoreTextIntro.text = "" + score;
         _scoreTextMain.text = "" + score;
         _scoreTextEnd.text = "" + score;
         PlayerPrefs.SetInt("Score",score);
         if (score>highScore)
         {
             highScore = score;
             PlayerPrefs.SetInt("highScore",highScore);
         }

         _highScoreTextEnd.text = "" + highScore;
     }
    public void TaptoPlay()
    {
        playerResetPos();
        playerController.RunningAnimation();
        _currentGameState = GameState.MainGame;
        CanvasSelect();
        inGameLevelText.text = $"Level {level}";

    }
    public void TryAgain()
    {
        score = 0;
        PlayerPrefs.GetInt("highScore");
        playerResetPos();
        DestroyObject();
        //camera reset
        //camPos reset 
        var temp = _cameraManager.camPos;
        temp.position = Vector3.zero;
        
        //main cam resetlenecek !!!!
        var temp2 = _cameraManager._camera;
        temp2.transform.position = Vector3.zero;
        temp2.transform.rotation = Quaternion.identity;
        
        inGameLevelText.text = $"Level {level}";
        SpawnCoins();
        SpawnUpgrades();
        SpawnWindows();
        _currentGameState = GameState.Prepare;
        CanvasSelect();
        
    }
    public void NextLevel()
    {
        
        level += 1;
        score = 0;
        PlayerPrefs.GetInt("highScore");

        playerResetPos();
        DestroyObject();
        var temp = _cameraManager.camPos;
        temp.position = Vector3.zero;
        
        var temp2 = _cameraManager._camera;
        temp2.transform.position = Vector3.zero;
        temp2.transform.rotation = Quaternion.identity;
        
        SpawnCoins();
        SpawnUpgrades();
        SpawnWindows();
        _currentGameState = GameState.Prepare;
        CanvasSelect();
        playerController.IdleAnimation();

    }
    public void CanvasSelect()
    {
        if (introCanvas.gameObject.activeInHierarchy == false)
        {
            introCanvas.gameObject.SetActive(true);
            inGameCanvas.gameObject.SetActive(false);
            endGameCanvas.gameObject.SetActive(false);
            deadGameCanvas.gameObject.SetActive(false);
        }
        else
        {
            introCanvas.gameObject.SetActive(false);
            inGameCanvas.gameObject.SetActive(true);
            endGameCanvas.gameObject.SetActive(false);
            deadGameCanvas.gameObject.SetActive(false);
        }
    }

    public void EndCanvas()
    {
        introCanvas.gameObject.SetActive(false);
        inGameCanvas.gameObject.SetActive(false);
        endGameCanvas.gameObject.SetActive(true);
        deadGameCanvas.gameObject.SetActive(false);
    }
  
    public void SpawnCoins()
    {
        for (int j = 1; j < 8; j++)
        {
            for (int i = 0; i < 10; i+=2)
            {
                //orta
                collectables.Add(Instantiate(collectablePrefab, new Vector3(0, -1.2f, i + 40f*j), Quaternion.identity));
                collectables.Add(Instantiate(collectablePrefab, new Vector3(0, -1.2f, i + 20f*j), Quaternion.identity));
            }
            for (int i = 0; i < 10; i+=2)
            {
                //ensol
                collectables.Add(Instantiate(collectablePrefab, new Vector3(-2f, -1.2f, i + 20f*j), Quaternion.identity));
                collectables.Add( Instantiate(collectablePrefab, new Vector3(-2f, -1.2f, i + 35f*j), Quaternion.identity));
                collectables.Add(Instantiate(collectablePrefab, new Vector3(-2f, -1.2f, i + 50f*j), Quaternion.identity));
            }
            for (int i = 0; i < 10; i+=2)
            {
                //ensağ
                collectables.Add(Instantiate(collectablePrefab, new Vector3(1.5f, -1.2f, i + 28.22f*j), Quaternion.identity));
                collectables.Add(Instantiate(collectablePrefab, new Vector3(1.5f, -1.2f, i + 50f*j), Quaternion.identity));
                collectables.Add(Instantiate(collectablePrefab, new Vector3(1.5f, -1.2f, i + 43.22f*j), Quaternion.identity));
                collectables.Add(Instantiate(collectablePrefab, new Vector3(1.5f, -1.2f, i + 38.22f*j), Quaternion.identity));
            }
        }
    }

        public void SpawnUpgrades()
    {
        
            for (int i = 20; i < 380; i+=40)
            {
                var seed = UnityEngine.Random.Range(0, 100);
                if(seed > 70) {
                    speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(0, -1.6f, i), Quaternion.identity));
                    if(seed > 90 && i> 10) {
                        upgrades.Add(Instantiate(upgradePrefab, new Vector3(0, -1.2f, i), Quaternion.identity));
                    }
                }
            }
            for (int i = 20; i < 380; i+=40)
            {
                 var seed = UnityEngine.Random.Range(0, 100);
                if(seed > 70) {
                    speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(-1.5f, -1.6f, i), Quaternion.identity));
                    if(seed > 90 && i> 10) {
                        upgrades.Add(Instantiate(upgradePrefab, new Vector3(-1.5f, -1.2f, i), Quaternion.identity));
                    }
                }
               
            }
            for (int i = 20; i < 380; i+=40)
            {
                var seed = UnityEngine.Random.Range(0, 100);
                if(seed > 70) {
                    speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(1.5f, -1.6f, i), Quaternion.identity));
                    if(seed > 90 && i> 10) {
                        upgrades.Add(Instantiate(upgradePrefab, new Vector3(1.5f, -1.2f, i), Quaternion.identity));
                    }
                }
            }
            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(1.5f, -1.6f, 25), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(1.5f, -1.2f, 25), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(1.5f, -1.6f, 35), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(1.5f, -1.2f, 35), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(0, -1.6f, 55), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(0, -1.2f, 55), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(-1.5f, -1.6f, 75), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(-1.5f, -1.2f, 75), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(1.6f, -1.6f, 105), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(1.5f, -1.2f, 105), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(0, -1.6f, 130), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(0, -1.2f, 130), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(0, -1.6f, 155), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(0, -1.2f, 155), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(-1.6f, -1.6f, 175), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(-1.6f, -1.2f, 175), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(0, -1.6f, 195), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(0, -1.2f, 195), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(1.5f, -1.6f, 225), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(1.5f, -1.2f, 225), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(1.5f, -1.6f, 235), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(1.5f, -1.2f, 235), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(0, -1.6f, 255), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(0, -1.2f, 255), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(-1.5f, -1.6f, 275), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(-1.5f, -1.2f, 275), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(1.6f, -1.6f, 305), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(1.5f, -1.2f, 305), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(0, -1.6f, 330), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(0, -1.2f, 330), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(0, -1.6f, 355), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(0, -1.2f, 355), Quaternion.identity));

            speedBoosts.Add(Instantiate(speedBoostPrefab, new Vector3(-1.6f, -1.6f, 375), Quaternion.identity));
            upgrades.Add(Instantiate(upgradePrefab, new Vector3(-1.6f, -1.2f, 375), Quaternion.identity));
       
    }
    
    public void SpawnWindows()
    {
        for (int i = 0; i < level; i++)
        {
            var spawnDistance = (int)UnityEngine.Random.Range(3, 40) * 10;
            var spawnSide = (int)UnityEngine.Random.Range(0, 3);
            if(spawnSide == 0) {
                upgrades.Add(Instantiate(windowTypes[0], new Vector3(1.6f, 0, spawnDistance), Quaternion.identity));
            } else if(spawnSide == 1) {
                upgrades.Add(Instantiate(windowTypes[0], new Vector3(0, 0, spawnDistance), Quaternion.identity));
            } else if(spawnSide == 2) {
                upgrades.Add(Instantiate(windowTypes[0], new Vector3(-1.6f, 0, spawnDistance), Quaternion.identity));
            }
        }
        for (int i = 1; i < level; i++)
        {
            var spawnDistance = (int)UnityEngine.Random.Range(3, 40) * 10;
            var spawnSide = (int)UnityEngine.Random.Range(0, 3);
            if(spawnSide == 0) {
                upgrades.Add(Instantiate(windowTypes[1], new Vector3(1.6f, 0, spawnDistance), Quaternion.identity));
            } else if(spawnSide == 1) {
                upgrades.Add(Instantiate(windowTypes[1], new Vector3(0, 0, spawnDistance), Quaternion.identity));
            } else if(spawnSide == 2) {
                upgrades.Add(Instantiate(windowTypes[1], new Vector3(-1.6f, 0, spawnDistance), Quaternion.identity));
            }
        }
        for (int i = 2; i < level; i++)
        {
           
            var spawnDistance = (int)UnityEngine.Random.Range(3, 38) * 10;
            var spawnSide = (int)UnityEngine.Random.Range(0, 3);
            if(spawnSide == 0) {
                upgrades.Add(Instantiate(windowTypes[2], new Vector3(1.6f, 0, spawnDistance), Quaternion.identity));
            } else if(spawnSide == 1) {
                upgrades.Add(Instantiate(windowTypes[2], new Vector3(0, 0, spawnDistance), Quaternion.identity));
            } else if(spawnSide == 2) {
                upgrades.Add(Instantiate(windowTypes[2], new Vector3(-1.6f, 0, spawnDistance), Quaternion.identity));
            }
        }
    }

    public void DestroyObject()
    {
        foreach (var item in collectables.ToList())
        {
            collectables.Remove(item);
            Destroy(item.gameObject);
        }
        foreach (var item in speedBoosts.ToList())
        {
            speedBoosts.Remove(item);
            Destroy(item.gameObject);
        }        
        foreach (var item in upgrades.ToList())
        {
            upgrades.Remove(item);
            Destroy(item.gameObject);
        }
        foreach (var item in windows.ToList())
        {
            windows.Remove(item);
            Destroy(item.gameObject);
        }
    }
    
}
