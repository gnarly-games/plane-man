﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            var player = other.gameObject.GetComponent<PlayerController>();
            player.Rocket++;
            var rocketIdx = (int)player.Rocket / 2;
            if(rocketIdx < 9 & rocketIdx >= 0) {
                player.SelectRocket(rocketIdx);
            }
            
            Destroy(gameObject);
        }
    }
}
