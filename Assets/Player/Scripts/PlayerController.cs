using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public enum RoadType
{
    Ice,
    Fire,
    Water,
    Dirt,
    SpeedBoost
}
public class PlayerController : MonoBehaviour
{
    public int speed;
    private int speedBoost = 1;
    private Rigidbody _rigidbody;
    private Animator _animator;
    private Camera _cam;
    public bool isInjured = true;

    public float maxDistanceX;
    private float _dirX;
    private float _mousePosX;
    public float swipeValue;
    bool dragging = false;
    RoadType currentRoad = RoadType.Dirt;
    private float currentSpeedBoost = 1f;
    bool slowdown;
    public bool launched = false;
    private float fireStart;
    private float fireEnd;
    private Quaternion currentAngle;
    private bool finished;
    private bool addPush;
    public List<GameObject> Rockets;
    public int Rocket;
    private Vector3 flyDir = Vector3.forward;
     private Vector3 flyDirTemp = Vector3.forward;
    public float boostDuration = 0;
    public float speedUpDuration;
    private void Awake()
    {
        Physics.gravity = new Vector3(0, -2, 0);
        currentAngle = transform.rotation;
        _rigidbody = GetComponent<Rigidbody>();
        _animator = gameObject.GetComponentInChildren<Animator>();
        _cam = Camera.main;
        Rocket = 0;
    }

    public void SelectRocket(int index) {
        foreach (var rocket in Rockets)
        {
            rocket.SetActive(false);
        }
        Rockets[index].SetActive(true);
    }
    public void IdleAnimation()
    {
        SelectRocket(9);
        currentRoad = RoadType.Dirt;
        _animator.SetTrigger("Idle");
        launched = false;
        finished = false;
        speedBoost = 2;
        currentSpeedBoost = 2.2f;
        Rocket = 0;
        transform.eulerAngles = Vector3.zero;

    }
    public void RunningAnimation()
    {
        _animator.SetTrigger("Run");
        isInjured = false;
    }
   public void DeadAnimation()
    {
        //_animator.SetTrigger("Dead");
        GameManager.manager.deadGameCanvas.gameObject.SetActive(true);
        GameManager.manager.inGameCanvas.gameObject.SetActive(false);
    }

    public void DanceAnimation()
    {
        //_animator.SetTrigger("Dance");
    }

   public void SliderMove()
    {
    
      Vector3 dir = transform.forward * (Time.deltaTime * speed);

        if(launched) {
            gameObject.transform.eulerAngles = Vector3.zero;
            var pos = gameObject.transform.position;
            pos.x = 0;
            gameObject.transform.position = pos;
            return;
        }
      if (Input.GetMouseButtonDown(0))
        {
            Vector3 playerPos = transform.position;
            _dirX = playerPos.x;
            _mousePosX = _cam.ScreenToViewportPoint(Input.mousePosition).x;
            dragging = true;
        }

        if (dragging)
        {
            float newMousePosX = _cam.ScreenToViewportPoint(Input.mousePosition).x;
            float distance = newMousePosX - _mousePosX;
            float posX = _dirX + (distance * swipeValue);
            
            Vector3 pos = transform.position;
            pos.x = posX;
            pos.x = Mathf.Clamp(pos.x, -maxDistanceX, maxDistanceX);
            if(currentRoad != RoadType.Dirt) {
                var angle = gameObject.transform.eulerAngles;
                angle.z = pos.x * 10f;
                gameObject.transform.eulerAngles = angle;
            }
            transform.position = pos;
        }

        if(Input.GetMouseButtonUp(0))
        {
            transform.Translate(dir);
            dragging = false;
        }

    }
   

    public void PlayerMove()
    {   
        var direction = flyDirTemp;
        if(finished) {
            speedBoost = 0;
            _rigidbody.velocity = Vector3.zero;
            return;
        }
        var temp = direction * (Time.deltaTime * speed * currentSpeedBoost);
        if(launched && Time.time > fireEnd) {
            addPush = true;
            temp +=  Vector3.down * Time.deltaTime * 10;
        }
         
        transform.Translate(temp); 
    }
    void Update() {
        if(Time.time > fireStart && launched) {
            currentSpeedBoost = 18;
        }
        if(Time.time > fireEnd && launched){
            currentSpeedBoost = 9;
        }
        currentSpeedBoost = Mathf.Lerp(currentSpeedBoost, speedBoost, Time.deltaTime*speedUpDuration);
        flyDirTemp = Vector3.Lerp(flyDirTemp, flyDir, Time.deltaTime*2);
       
    }

    public void IceRoad()
    {
        _animator.SetTrigger("FlySlow");
  
        if(currentRoad == RoadType.Ice){
            return;
        }

        currentRoad = RoadType.Ice;
        speedBoost = 4;
        currentSpeedBoost = 5f;
    }
    public void SpeedRoad()
    {
        _animator.SetTrigger("FlyFast");

        currentRoad = RoadType.SpeedBoost;
        currentSpeedBoost = 8f;
    }
    public void DirtRoad()
    {
  
        if(currentRoad == RoadType.Dirt){
            return;
        }

        currentRoad = RoadType.Dirt;
        _animator.SetTrigger("Run");
        currentSpeedBoost = 1;
        speedBoost = 1;
    }
    public void FireRoad()
    {
  
        if(currentRoad == RoadType.Fire){
            return;
        }

        currentRoad = RoadType.Fire;
        _animator.SetTrigger("Run");
        currentSpeedBoost = 3;
    }
    private void OnTriggerEnter(Collider other)
    {   
        if(other.gameObject.tag == "Ice") {
            IceRoad();
        }
        else if(other.gameObject.tag == "Dirt") {
            DirtRoad();
        }
        else if(other.gameObject.tag == "Fire") {
            FireRoad();
        }
        else if(other.gameObject.tag == "SpeedBoost") {
            slowdown = false;
            SpeedRoad();
        }
        else if(other.gameObject.tag == "LaunchPad"){
            flyDir = new Vector3(0, 0.6f, 1f);
        }
        if (other.gameObject.tag == "Finish")
        {

            launched = false;
            finished = true;
            speedBoost = 0;
            currentSpeedBoost = 1;
            var multi = other.gameObject.GetComponent<Finisher>().Multiplier;
            GameManager.manager.score *= multi;
            GameManager.manager.EndCanvas();
            GameManager.manager.CurrentGameState = GameManager.GameState.EndGame;
        }
    }
    private void OnTriggerExit(Collider other)
    {   

        if(other.gameObject.tag == "SpeedBoost") {
            slowdown = true;
        }
        if(other.gameObject.tag == "Window"){
            other.gameObject.GetComponent<BreakableWindow>().breakWindow();
            var damage = (int)other.gameObject.GetComponent<BreakableWindow>().health;
            var newRocketCount = Rocket - damage;
            if(damage > Rocket) {
                Rocket = 0;
            } else {
                Rocket = newRocketCount;
            }
            var rocketIdx = (int)Rocket / 2;
            if(rocketIdx < 9 & rocketIdx >= 0) {
                SelectRocket(rocketIdx);
            }
            currentSpeedBoost = 4;
            slowdown = true;
            Destroy(other.gameObject, 2f);

        }
        
        if(other.gameObject.tag == "LaunchPad") {
            launched = true;
            fireStart = Time.time;
            
            var totalRocket = GameManager.manager.upgrades.Count;
            var collectedRocket = Rocket;
            var successRate = collectedRocket / (float)totalRocket;

            fireEnd = fireStart + successRate * 3f;
            boostDuration = successRate;
            flyDir = new Vector3(0, 0.0f, 1f);
           _animator.SetTrigger("FlyFast");
        }
    }
}
